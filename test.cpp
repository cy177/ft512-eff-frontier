#include "../../eigen-3.4.0/Eigen/Dense"
// #include <Eigen/Dense>
using namespace Eigen;
#include <iostream>

int main()
{
    Matrix3f m;
    m << 1, 2, 3,
        4, 5, 6,
        7, 8, 9;
    std::cout << m;
}