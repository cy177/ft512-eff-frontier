// #include "../../eigen-3.4.0/Eigen/Dense"
#include <Eigen/Dense>
using namespace Eigen;
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <cctype>
#include <iomanip>

using namespace std;

bool isDouble(string x)
{
    int i = 0;
    while (x[i] != 0)
    {
        if (!(isdigit(x[i]) || x[i] == '.' || x[i] == '-' || x[i] == 'e'))
        {
            return false;
        }
        i++;
    }
    return true;
}

double un_optimal(MatrixXd Corr, vector<double> averet, vector<double> stdev, double &ret)
{
    int num = Corr.rows();
    MatrixXd Cov(num, num);
    for (int i = 0; i < num; i++)
    {
        for (int j = 0; j < num; j++)
        {
            Cov(i, j) = Corr(i, j) * stdev[i] * stdev[j];
        }
    }
    MatrixXd A(2, num);
    for (int j = 0; j < num; j++)
    {
        A(0, j) = 1;
        A(1, j) = averet[j];
    }
    MatrixXd b(2, 1);
    b(0, 0) = 1;
    b(1, 0) = ret;
    MatrixXd AA(num + 2, num + 2);
    MatrixXd zerom(2, 2);
    zerom(0, 0) = 0;
    zerom(0, 1) = 0;
    zerom(1, 0) = 0;
    zerom(1, 1) = 0;

    AA << Cov, A.transpose(), A, zerom;
    MatrixXd bb(num + 2, 1);
    for (int i = 0; i < num; i++)
    {
        bb(i, 0) = 0;
    }
    bb(num, 0) = 1;
    bb(num + 1, 0) = ret;

    MatrixXd AA_inverse = AA.inverse();

    MatrixXd XX = AA_inverse * bb;
    MatrixXd weight(num, 1);
    for (int i = 0; i < num; i++)
    {
        weight(i, 0) = XX(i, 0);
    }
    MatrixXd result = weight.transpose() * Cov * weight;
    double volatility = sqrt(result(0, 0));

    return volatility;
}
double re_optimal(MatrixXd Corr, vector<double> averet, vector<double> stdev, double &ret)
{
    int num = Corr.rows();
    MatrixXd Cov(num, num);
    for (int i = 0; i < num; i++)
    {
        for (int j = 0; j < num; j++)
        {
            Cov(i, j) = Corr(i, j) * stdev[i] * stdev[j];
        }
    }
    MatrixXd A(2, num);
    for (int j = 0; j < num; j++)
    {
        A(0, j) = 1;
        A(1, j) = averet[j];
    }
    MatrixXd b(2, 1);
    b(0, 0) = 1;
    b(1, 0) = ret;
    MatrixXd AA(num + 2, num + 2);
    MatrixXd zerom(2, 2);
    zerom(0, 0) = 0;
    zerom(0, 1) = 0;
    zerom(1, 0) = 0;
    zerom(1, 1) = 0;

    AA << Cov, A.transpose(), A, zerom;
    MatrixXd bb(num + 2, 1);
    for (int i = 0; i < num; i++)
    {
        bb(i, 0) = 0;
    }
    bb(num, 0) = 1;
    bb(num + 1, 0) = ret;

    MatrixXd AA_inverse = AA.inverse();

    MatrixXd XX = AA_inverse * bb;
    MatrixXd weight(num, 1);
    for (int i = 0; i < num; i++)
    {
        weight(i, 0) = XX(i, 0);
    }
    //check for negative weights

    while (true)
    {
        int indicator = 1;
        int count = 0;
        MatrixXd C;
        //check for the most negative number
        // int minvalue = 0;
        // int index;
        // for (int i = 0; i < num; i++)
        // {
        //     if (weight(i, 0) < minvalue)
        //     {
        //         minvalue = weight(i, 0);
        //         index = i;
        //     }
        // }

        //if (weight(index, 0) < 0)
        for (int index = 0; index < num; index++)
        {
            if (weight(index, 0) < 0)
            {

                MatrixXd temp = MatrixXd::Zero(1, num);
                temp(0, index) = 1;
                MatrixXd T = C;
                C.resize(count + 1, num);
                if (count == 0)
                {
                    C << temp;
                }
                else
                {
                    C << T, temp;
                }
                count++;
                indicator = 0;
            }
        }

        if (indicator == 1)
        {
            break;
        }
        MatrixXd T;
        T = A;
        A.resize(T.rows() + C.rows(), num);
        A << T, C;
        T = bb;
        bb.resize(T.rows() + C.rows(), 1);
        bb << T, MatrixXd::Zero(C.rows(), 1);
        MatrixXd OO = MatrixXd::Zero(A.rows(), A.rows());
        MatrixXd K(Cov.rows() + A.rows(), Cov.rows() + A.rows());
        K << Cov, A.transpose(), A, OO;
        XX = K.inverse() * bb;
        for (int i = 0; i < num; i++)
        {
            weight(i, 0) = XX(i, 0);
        }
    }

    MatrixXd result = weight.transpose() * Cov * weight;
    double volatility = sqrt(result(0, 0));

    return volatility;
}

int main(int argc, char **argv)
{
    if (argc != 3 && argc != 4)
    {
        cerr << "wrong input number" << endl;
        return EXIT_FAILURE;
    }

    //check whether -r pass in
    bool r;
    string universe;
    string correlation;
    if (argc == 3)
    {
        r = false;
        universe = argv[1];
        correlation = argv[2];
    }
    if (argc == 4)
    {
        if (strcmp(argv[1], "-r") == 0)
        {
            r = true;
            universe = string(argv[2]);
            correlation = string(argv[3]);
        }
        else if (strcmp(argv[3], "-r") == 0)
        {
            r = true;
            universe = string(argv[1]);
            correlation = string(argv[2]);
        }
    }
    //read file and parse line
    ifstream input_u, input_c;
    input_u.open(universe);
    input_c.open(correlation);
    if (!input_u.is_open() || !input_c.is_open())
    { //check for opening file
        cerr << "Cannot open file" << endl;
        exit(EXIT_FAILURE);
    }

    //parse universe file
    string line_u;
    vector<string> Assetname;
    vector<double> avgreturn;
    vector<double> sigma;

    while (getline(input_u, line_u))
    {
        stringstream s(line_u);
        getline(s, line_u, ',');
        Assetname.push_back(line_u);

        getline(s, line_u, ',');
        if (!isDouble(line_u))
        {
            cerr << "wrong format for double" << endl;
            exit(EXIT_FAILURE);
        }

        avgreturn.push_back(stod(line_u));

        getline(s, line_u, ',');
        if (!isDouble(line_u))
        {
            cerr << "wrong format for double" << endl;
            exit(EXIT_FAILURE);
        }

        sigma.push_back(stod(line_u));
    }
    size_t assetsize = Assetname.size();

    //parse correlation file
    MatrixXd Corr(assetsize, assetsize);
    int i = 0;
    size_t j = 0;
    string line_c;
    while (getline(input_c, line_c))
    {
        stringstream ss(line_c);
        while (getline(ss, line_c, ','))
        {
            if (!isDouble(line_c))
            {
                cerr << "wrong format for double" << endl;
                exit(EXIT_FAILURE);
            }
            double corr_d = stod(line_c);
            Corr(i, j) = corr_d;
            j++;
        }
        i++;
        if (j != assetsize)
        {
            cerr << "ncol is smaller than the asset size" << endl;
            exit(EXIT_FAILURE);
        }
        j = 0;
    }
    if (i == 0)
    {
        cerr << "empty file" << endl;
        exit(EXIT_FAILURE);
    }
   
    //handling wrong format of the matrix!!!!!
    //if the matrix is not positive semidefinite
    for (unsigned int i = 0; i < assetsize; i++)
    { //Actually correlation matrix shoule be positive semidefinite
        for (unsigned int j = 0; j <= i; j++)
        {
            if (fabs(Corr(i, j) - Corr(j, i)) > 0.0001 || fabs(Corr(i, j)) > 1.0001)
            {
                cerr << "Correlation matrix has wrong mathematical format" << endl;
                exit(EXIT_FAILURE);
            }
        }
        if (fabs(Corr(i, i) - 1) > 0.0001)
        {
            cerr << "Corr(i,i) does not equal to 1" << endl; //handle Corr(i,i) != 1
            exit(EXIT_FAILURE);
        }
    }
    cout << "ROR,volatility" << endl;
    cout.setf(std::ios::fixed);
    if (r == false)
    {
        for (double l = 0.01; l <= 0.265; l += 0.01)
        {
            cout << fixed << setprecision(1) << l * 100 << "%,";
            cout << fixed << setprecision(2) << un_optimal(Corr, avgreturn, sigma, l) * 100 << "%" << endl;
        }
    }
    //(5) Step 2: Restricted case: return level between 1% and 26% in 1% increments.
    if (r == true)
    {
        for (double l = 0.01; l <= 0.265; l += 0.01)
        {
            cout << fixed << setprecision(1) << l * 100 << "%,";
            cout << fixed << setprecision(2) << re_optimal(Corr, avgreturn, sigma, l) * 100 << "%" << endl;
        }
    }
    cout.unsetf(std::ios::fixed);
    return EXIT_SUCCESS;
}